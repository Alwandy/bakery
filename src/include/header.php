<?php

session_start();

if(isset($_GET['source']) && !empty($_GET['source']))
{
	$_SESSION['source'] = preg_replace('/[^0-9a-zA-Z]/', '', $_GET['source']);
}
else
{
	$_SESSION['source'] = 'null';
}

$url = $_SERVER['REQUEST_URI'];
$parts = explode('/',$url);
$dir = $_SERVER['SERVER_NAME'];
for ($i = 0; $i < count($parts) - 1; $i++)
{
	$dir .= $parts[$i] . "/";
}

// IP Detect
if(isset($_SERVER["HTTP_CF_CONNECTING_IP"]))
{
	$real_ip_adress = $_SERVER["HTTP_CF_CONNECTING_IP"];
}
else if(isset($_SERVER['HTTP_CLIENT_IP']))
{
	$real_ip_adress = $_SERVER['HTTP_CLIENT_IP'];
}
else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
{
	$real_ip_adress = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
else
{
	$real_ip_adress = $_SERVER['REMOTE_ADDR'];
}

$_SESSION['ip'] = isset($real_ip_adress) ? $real_ip_adress : 'null';
// $data = geoip_record_by_name($_SESSION['ip']1);
$_SESSION['countryCode'] = isset($data['country_code']) ? $data['country_code'] : 'null';
$_SESSION['countryName'] = isset($data['country_name']) ? $data['country_name'] : 'null';

$detect = new Mobile_Detect;

?>