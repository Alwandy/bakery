<?php

/**
----------------------
  Application config
----------------------
**/

// Default timezone
date_default_timezone_set('Europe/London');

// App global
$app = (object) [];

// Config object
$app->config = (object) [
	'dir' => realpath(getcwd() . '/../'),

	'client' => (object) [
		'id' => 2,
		'name' => 'IA Property',
	],

	'campaign' => (object) [
		'name' => 'Sky Gardens'
	],

	'content' => (object) [
		'heading' => "TEST HEADING",
		'subheading' => "TEST SUBHEADING",
		'bullets' => (object) [
			'Bullet Testing #1',
			'Bullet Testing #2',
			'Bullet Testing #3',
			'Bullet Testing #4'
		]
	]
];

// Add autoloader
require_once($app->config->dir . '/vendor/autoload.php');

?>