/*
	API and test setup
	----------------------------------- */

	var browsersync = require('browser-sync'),
		gulp = require('gulp'),
		php = require('gulp-connect-php'),
		runSequence = require('run-sequence'),
		sass = require('gulp-ruby-sass') ,
	    notify = require("gulp-notify") ,
	    bower = require('gulp-bower'),
	    uglify = require('gulp-uglifyjs');

	var config = {
	     sassPath: './resources/sass',
	     bowerDir: './bower_components' ,
		publicPath: './src',
		resourcePath: './resources',

		jsdir: [
			'bower_components/jquery/dist/jquery.js',
			'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
			'resources/js/**/*.js'
		]
	}

/*
	Run Bower Tasks
	----------------------------------- */

	gulp.task('bower', function() { 
		return bower()
	         .pipe(gulp.dest(config.bowerDir)) 
			.on("error", notify.onError(function (error) {
                 return "Error: " + error.message;
             }));
	});

	gulp.task('fonts', function() { 
	    return gulp.src([
	    		config.bowerDir + '/font-awesome/fonts/**.*',
	    		config.bowerDir + '/bootstrap-sass/assets/fonts/**.*',
	    	]) 
	        .pipe(gulp.dest(config.publicPath + '/assets/fonts'))
	        .on("error", notify.onError(function (error) {
                 return "Error: " + error.message;
             })); 
	});

	gulp.task('css', function() { 
	    return gulp.src(config.sassPath + '/style.scss')
	         .pipe(sass({
	             style: 'compressed',
	             loadPath: [
	                 './resources/sass',
	                 config.bowerDir + '/bootstrap-sass/assets/stylesheets',
	                 config.bowerDir + '/font-awesome/scss',
	             ]
	         }) 
	        .on("error", notify.onError(function (error) {
                 return "Error: " + error.message;
             }))) 
	         .pipe(gulp.dest(config.publicPath + '/assets/css')); 
	});

	gulp.task('js', function() {
		gulp.src(config.jsdir)
			.pipe(uglify('app.min.js', {
      			outSourceMap: true
    		}))
			.pipe(gulp.dest(config.publicPath + '/assets/js'))
	});

	 gulp.task('watch', function() {
	     gulp.watch(config.sassPath + '/**/*.scss', ['css']); 
	     gulp.watch('resources/js/**/*.js', ['js']); 
	});

  	gulp.task('assets', ['js', 'fonts', 'css']);
	  gulp.task('default', ['bower', 'fonts', 'css']);
	
/*
	Local PHP server
	----------------------------------- */

	gulp.task('php', function() {

		var options = {
			base: 'src/',
			// bin: '/Applications/MAMP/bin/php/php5.6.10/bin/php',
			port: 8000,
			keepalive: true
		}

		return php.server(options);
	});

/*
	Open browser
	----------------------------------- */

	gulp.task('browsersync', ['php'], function() {

		var options = {
			browser: 'google chrome',
			proxy: '127.0.0.1:8000',
			notify: true
		}

		browsersync(options);
	});

/*
	Core tasks
	----------------------------------- */

	// Open browser
	gulp.task('dev', function(callback) {
		return runSequence('browsersync');
	});
