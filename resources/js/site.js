function referenceload() {
    var x = document.getElementById("reference")
    x.innerHTML = "<code>Reference number: " + Math.floor((Math.random() * 10000) + 1) + "</code>";
}

$( document ).ready(function() {
	$('#cakeslide').owlCarousel({
		loop:true,
		margin:250,
		nav:false,
		responsive:{
			0:{
				items:1
			},
		600:{
				items:3
			},
		1000:{
				items:5
			}
		}
	});

	$('#order').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var ordertype = button.data('order') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find('.modal-title').text('Cake Ordering - ' + ordertype)
	  modal.find('.modal-body #orders input').val(ordertype)
	});

	$("#orderForm").validate({
		rules: {
			customername: "required",
			address: "required",
			email: {
				required: true,
				email: true
			},
			phone: "required"
		},
		messages: {
			customername: "* Please enter your name",
			address: "* Please enter your address",
			email: "* Please enter a valid email address",
			phone: "* Please enter your phone number"
		},
		errorLabelContainer: $("#orderForm div.error"),
		submitHandler: function(form) {
			$('#order').modal('hide');
			$('#ordersent').modal('show');
			referenceload();
		}
	});

});