Alwandy's Cake Shop
====

### Initial setup

As root/adminstrator:

```
npm install -g gulp
```

Install dependencies automatically by running:
```
npm install
```

Install bower dependencies:
```
gulp bower
```

### Building
All files for deployment copied to `/src/`